﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace ManajemenStok.Helper
{
    public static class ConfigModel
    {
        public static bool Develop { get { return false; } }
        public static string UnitName { get { return "Rumah Sakit Wisma Prashanti"; } }
        public static string AppName { get { return "Manajemen Stok"; } }
        public static string Version { get { return "0.1"; } }
    }
}